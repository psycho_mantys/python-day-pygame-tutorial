#!/usr/bin/env python

import pygame

COLOR=dict([])
COLOR["black"] = pygame.Color(  0,   0,   0)
COLOR["white"] = pygame.Color(255, 255, 255)
COLOR["blue"]  = pygame.Color( 50,  50, 255)
COLOR["green"] = pygame.Color( 50, 255,  50)
COLOR["red"]   = pygame.Color(255,  50,  50)
COLOR["yellow"]= pygame.Color(255, 255,  50)
COLOR["pink"]  = pygame.Color(255,  50, 255)
COLOR["transparent"] = pygame.Color(44,3,195)

