#!/usr/bin/env python

import pygame
from load_image import *

class Tile(pygame.sprite.Sprite):
	name=None

	rect=None
	image=None

	velocity=(0,0)

	x=0.0
	y=0.0

	def __init__(self, filename, x=0, y=0, velocity=(0,0)):
		pygame.sprite.Sprite.__init__(self)

		self.image=load_image(filename, -1)
		self.rect=self.image.get_rect()

		self.rect.x=x
		self.rect.y=y

		self.y=y
		self.x=x

		self.velocity=velocity

	def draw(self, screen):
		return screen.blit(self.image, self.rect)

	def update(self, dt):
		self.rect.x=self.x
		self.rect.y=self.y

	def move(self, x=0, y=0):
		self.rect.move_ip(x, y)
		self.x=self.rect.x
		self.y=self.rect.y

	def dt_x_move(self, dt):
		dx=dt*(self.velocity[0]/1000)
		self.x+=dx
		self.rect.x=self.x

	def dt_y_move(self, dt):
		dy=dt*(self.velocity[1]/1000)
		self.y+=dy
		self.rect.y=self.y

	def dt_move(self, dt):
		self.dt_x_move(dt)
		self.dt_y_move(dt)

	def update(self, dt):
		self.dt_move(dt)

