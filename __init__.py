#!/usr/bin/env python

import pygame

from random import randint

import tile
from colors import *

class Frog(object):
	screen_mode=(800,400)
	screen=None
	clock=None
	window_caption="Python Day - FPS: {:.2f}"

	dt=0
	car_tick=0
	car_spawn_time=750
	car_base_velocity=(0,40)

	done=False
	fps=30
	tile_size=40

	def __init__(self):
		successes, failures=pygame.init()
		print("Initializing pygame: {0} successes and {1} failures.".format(successes, failures))

		self.clock=pygame.time.Clock()

		self.screen=pygame.display.set_mode( self.screen_mode )
		pygame.display.set_caption(self.window_caption)

		self.player=tile.Tile("media/img/frog.png", 0, 200, (0,0))
		self.player.name='Frog'

		self.layer_front=pygame.sprite.Group()
		self.layer_background=pygame.sprite.Group()

		self.layer_front.add(self.player)

		flip=True
		for x in range(0, self.screen_mode[0], self.tile_size):
			if flip:
				t=tile.Tile("media/img/grass_field.png", x)
			else:
				t=tile.Tile("media/img/road_field.png", x)
			flip=not flip
			self.layer_background.add(t)


	def __del__(self):
		pygame.quit()


	def main_loop(self):
		while not self.done:
			self.event_handler()
			self.game_logic()
			self.step_physics()
			self.render()

	def event_handler(self):
		for event in pygame.event.get():
			# Trata evento QUIT
			if event.type==pygame.QUIT:
				self.done=True
			elif event.type==pygame.KEYDOWN:
				print(pygame.key.name(event.key))
				print(event.key)
				if event.key==pygame.K_ESCAPE:
					self.done=True
				if event.key==32 or event.key==pygame.K_RIGHT:
					self.player.move(self.tile_size/4)
				if event.key==pygame.K_DOWN:
					self.player.move(0,self.tile_size/4)
				if event.key==pygame.K_UP:
					self.player.move(0,-self.tile_size/4)


	def game_logic(self):
		self.car_tick+=self.dt
		if self.car_spawn_time<self.car_tick:
			self.car_tick=0
			img="media/img/car_0"+str(randint(1,3))+".png"
			x=(1+2*randint(0,(self.screen_mode[0]/self.tile_size)/2))*(self.tile_size)
			car=tile.Tile(img, x, 0, self.car_base_velocity)
			y=-car.image.get_rect().height
			car.y=y
			while pygame.sprite.spritecollide(car, self.layer_front, False):
				x=(1+2*randint(0,(self.screen_mode[0]/self.tile_size)/2))*(self.tile_size)
				car=tile.Tile(img, x, y, self.car_base_velocity)
			self.layer_front.add(car)

		# Verifica se o jogador bateu em algo da layer_front
		collisions=pygame.sprite.spritecollide(self.player, self.layer_front, False)
		for collision in collisions:
			# Se ele bateu em alquem que não é um sapo, acaba o jogo
			if collision.name!='Frog':
				self.done=True

		# Se o carro saiu da tela, remove ele de todos os grupos
		for car in self.layer_front:
			if self.screen_mode[1]<car.y:
				car.kill()

		# Verifica se o sapo passou dos limites
		if self.screen_mode[0]<self.player.x:
			self.done=True
		if self.screen_mode[1]<self.player.y:
			self.done=True

		pygame.display.set_caption(self.window_caption.format(self.clock.get_fps()))

	def step_physics(self):
		self.layer_front.update(self.dt)
		self.layer_background.update(self.dt)

	def render(self):
		self.screen.fill(COLOR["black"])

		self.layer_background.draw(self.screen)
		self.layer_front.draw(self.screen)

		pygame.display.flip()
		self.dt=self.clock.tick(self.fps)

if __name__ == "__main__":
	Frog().main_loop()

